import discord
import prefs
from discord.ext import commands

bot = commands.Bot(
	command_prefix = prefs.prefix,
	case_insensitive = True
)

cogs = [
	"cogs.roll",
	"cogs.util"
]

@bot.event
async def on_ready():
	bot.load_extension("error_handling")
	for cog in cogs:
		bot.load_extension(cog)
		print(cog + " is loaded!")

	print('{0.user} is online and ready to fight.'.format(bot))

bot.run(prefs.token)
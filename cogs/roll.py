#rolling module for handling raw rolls ie "n!roll 2d6h1+5" , and handling the rolling based on input from a sheet in the future, ie "n!c atk longsword"
import prefs
import random
import re
from discord.ext.commands import bot

class Roll:
	def __init__(self, bot_client):
		self.bot = bot_client
		self.regex = re.compile('(?P<join>[\+\-])?(?:(?:(?P<dicecount>\d+)?d(?P<diesize>\d+))|(?P<modifier>\d+))(?:r(?P<reroll>\d+))?(?:(?P<keep>[hl])(?P<keepcount>\d+))?')


	@bot.command()
	async def roll(self, message, *, arg=""):
		"""
		Command for rolling dice with many different modifiers and rules.

		Usage:
			,roll <arguments>
			If no options are provided, Niner will roll a twenty sided die and give you the result. Capitalizations and spaces are ignored.

		Examples:
			,roll
			outputs a d20 roll

			,roll 4d6 + 2
			rolls 4 6-sided dice, then adds 2.

			,roll 2d20h1 - 1 + 2d4l1
			rolls 2 20-sided dice, keeps the high roll, subtracts 1, then rolls 2 4-sided die, keeps the lower one, and adds to the previous result.

			,roll 2d6r2
			rolls 2 6-sided die, and if the initial roll was 2 or lower, re-rolls the die.
			
		Arguement Format:
			<n> "d" <n> "h or l" <n> "r" <n> "+ or -" then another set

		Huge thanks to TBTerra#5677, Roxxers#7443, and Theo#7777 for helping me figure this all out.
		"""
		# TODO: make that not hardcode the fuckin prefix but it works for now so #fuckit
		
		arg = arg.lower().replace(' ', '')
		results = [r.groupdict() for r in self.regex.finditer(arg)]

		if len(results) is 0:
			await message.channel.send(f"Roll: 1d20 = {random.randint(1,20)}")
			return

		outstring = f"Roll: {arg} = "
		individual_rolls = []
		output = 0

		for i, result in enumerate(results):

			outbounds = "HA! I got 9 intelligence. You really think I can figure that mess out?"
			if result["dicecount"] and int(result["dicecount"]) > prefs.max_diecount:
				await message.channel.send(outbounds)
				return

			if result["diesize"] and int(result["diesize"]) > prefs.max_diesize:
				await message.channel.send(outbounds)
				return

			if result["modifier"] and int(result["modifier"]) > prefs.max_modifier:
				await message.channel.send(outbounds)
				return

			if result["keepcount"] and int(result["keepcount"]) > int(result["dicecount"]):
				await message.channel.send(outbounds)
				return

			if result["reroll"] and int(result["reroll"]) > int(result["diesize"]):
				await message.channel.send(outbounds)
				return

			group_output = 0

			if result["join"]:
				outstring += result["join"] + " "


			if result["modifier"]:
				group_output = int(result["modifier"])
				individual_rolls.append([group_output])


			if result["diesize"]:
				count = 1 if (result["dicecount"] is None) else int(result["dicecount"])
				size = int(result["diesize"])
				individual_rolls.append([])
				for _ in range(0, count):
					x = random.randint(1,size)

					if result["reroll"] and x <= int(result["reroll"]):
						x = random.randint(1,size)

					individual_rolls[i].append(x)
					group_output += x

				individual_rolls[i].sort(reverse=True)
				if result["keep"]:

					keep_high = result["keep"] == "h"
					keep_count = int(result["keepcount"])

					individual_rolls[i] = individual_rolls[i][:keep_count] if keep_high else individual_rolls[i][-keep_count:]
					group_output = sum(individual_rolls[i])

			outstring += f"{individual_rolls[i]} "


			if result["join"]:
				output += group_output if (result["join"] is "+") else -group_output

			else:
				output = group_output

		await message.channel.send(outstring + f"= {output}")

def setup(bot_client):
	bot_client.add_cog(Roll(bot_client))
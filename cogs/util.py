# Utility Commands module, for help, about, avatar, nickname change, invite link to server and 0ath
import discord
import prefs
from discord.ext.commands import bot

class Util:
	def __init__(self, bot_client):
		self.bot = bot_client

	@bot.command()
	async def about(self, message):
		"""
		Shows some basic info about Niner!
		"""
		embed = discord.Embed(title="About", colour=discord.Colour(0xff9f00), description="A RPG assistant bot created by Summer Broadway. Named after their foul-mouthed human fighter.")
		embed.set_thumbnail(url=self.bot.user.avatar_url)
		embed.set_author(name="Niner")
		embed.set_footer(text="Niner is licensed under the MIT License")
		embed.add_field(name="Links", value="[Gitlab](https://gitlab.com/Sacagahontas/Niner)\n[Trello](https://trello.com/b/2ETquKIY/niner)\n[Issue Reporting](https://gitlab.com/Sacagahontas/Niner/issues)")
		embed.add_field(name="Prefix", value=prefs.prefix, inline=True)
		embed.add_field(name="Owner", value=f"<@{prefs.owner}>", inline=True)
		embed.add_field(name="Bot Version", value="Indev", inline=True)
		embed.add_field(name="Discord.py Version", value="1.0.0a", inline=True)
	
		await message.channel.send(embed=embed)

def setup(bot_client):
	bot_client.add_cog(Util(bot_client))
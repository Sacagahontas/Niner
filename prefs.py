import configparser

config = configparser.ConfigParser()
config.read("config.ini")

owner = config["bot"]["owner"]
prefix = config["bot"]["prefix"]
token = config["bot"]["token"]

max_diecount = int(config["roll"]["max_diecount"])
max_diesize = int(config["roll"]["max_diesize"])
max_modifier = int(config["roll"]["max_modifier"])